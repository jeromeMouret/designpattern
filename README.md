# Desing pattern

Singleton - factory - abstract factory - builder - ...


## Singleton



## factory

![Screenshot](factory-1.png)

![Screenshot](factory-2.png)

## abstract factory

![Screenshot](abstract-factory-1.png)

![Screenshot](abstract-factory-2.png)

![Screenshot](abstract-factory-3.png)

![Screenshot](abstract-factory-4.png)

## builder



## ...


