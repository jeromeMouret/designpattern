﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace composite
{
    public class Directreur : ISalarie
    {
        private String nom;
        private String prenom;
        private String service;

        public Directreur(String Prenom, String nom, String service)
        {
            this.nom = nom;
            this.prenom = Prenom;
            this.service = service;
            this.Salaries = new List<ISalarie>();
        }

        public List<ISalarie> Salaries
        {
            get; set;
        }

        public void addSalarie(ISalarie salarie)
        {
            Salaries.Add(salarie);
        }

        public void afficher()
        {
            Console.WriteLine("Directeur : " + this.nom + " " + this.prenom + ", " + this.service + ".");
            foreach (ISalarie Salarie in Salaries)
            {
                Salarie.afficher();
            }
        }
    }
}