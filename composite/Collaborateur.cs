﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace composite
{
    public class Collaborateur : ISalarie
    {
        private String nom;
        private String prenom;

        public Collaborateur(String Prenom, String nom)
        {
            this.nom = nom;
            this.prenom = Prenom;
        }

        public void afficher()
        {
            Console.WriteLine("-----Collaborateur : " + this.nom + " " + this.prenom + " .");
        }

    }
}