﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace composite
{
    class Program
    {
        static void Main(string[] args)
        {
            Directreur dir1 = new Directreur("Michel", "Maldo", "PDG");

            ISalarie colab1 = new Collaborateur("Max", "COUR");


            Directreur dir2 = new Directreur("Jerome", "MOURET", "SI");

            ISalarie colab2 = new Collaborateur("Raf", "AL");

            ISalarie colab3 = new Collaborateur("Ju", "CHAP");

            dir1.addSalarie(colab1);

            dir1.addSalarie(dir2);

            dir2.addSalarie(colab2);
            dir2.addSalarie(colab3);

            dir1.afficher();

            int a = 2;
        }
    }
}
