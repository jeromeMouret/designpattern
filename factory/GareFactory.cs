﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace factory

{
    public class GareFactory : ProprietéFactory
    {
        public override Propriete creerPropriete(String libelle, int prix)
        {
            Propriete gare = new Gare(libelle, prix);
            return gare;
        }
    }
}