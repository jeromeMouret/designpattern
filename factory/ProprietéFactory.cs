﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace factory

{
    public abstract class ProprietéFactory
    {
        public abstract Propriete creerPropriete(String libelle, int prix);

    }
}