﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factory

{
    class Program
    {
        static void Main(string[] args)
        {
            ProprietéFactory TerrainFact = new TerrainFactory();
            TerrainFact.creerPropriete("Terrain", 200).afficher();

            ProprietéFactory GareFact = new GareFactory();
            GareFact.creerPropriete("Gare", 400).afficher();

            ProprietéFactory Compagniefact = new CompagnieFactory();
            Compagniefact.creerPropriete("Compagnie des eaux", 500).afficher();

            int a = 2;
        }
    }
}
