﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace factory

{
    public class Terrain : Propriete
    {

        public Terrain(String libelle, int prix):base(libelle, prix)
        { 
           
        }

        public override void afficher()
        {
            Console.WriteLine(this.libelle + "  a " + this.prix + " euros.");
     
        }
    }
}