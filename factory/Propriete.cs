﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace factory
{
    public abstract class Propriete
    {
        protected int prix;
        protected String libelle;

        public Propriete(String libelle, int prix)
        {
            this.prix = prix;
            this.libelle = libelle;
        }

        public abstract void afficher();

    }
}