﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace factory

{
    public class TerrainFactory : ProprietéFactory
    {
        public override Propriete creerPropriete(String libelle,  int prix)
        {
            Propriete terrain = new Terrain(libelle, prix);
            return terrain;
        }
    }
}