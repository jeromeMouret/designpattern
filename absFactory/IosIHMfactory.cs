﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace absFactory
{
    public class IosIHMfactory : AbstractIHMfactory
    {
        public override AbstractButton getButton()
        {
            IosButton button = new IosButton();

            return button;
        }

        public override AbstractCheckBox getCheckBox()
        {
            IosCheckBox checkButton = new IosCheckBox();

            return checkButton;
        }

        public override AbstractTextBox getTextBox()
        {
            IosTextBox textBox = new IosTextBox();

            return textBox;
        }
    }
}