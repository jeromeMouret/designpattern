﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace absFactory
{
    public class WindowsFormsIHMfactory : AbstractIHMfactory
    {
        public override AbstractButton getButton()
        {
            WindowsFormButton button = new WindowsFormButton();

            return button;
        }

        public override AbstractCheckBox getCheckBox()
        {
            WindowsFormCheckBox checkButton = new WindowsFormCheckBox();

            return checkButton;
        }

        public override AbstractTextBox getTextBox()
        {
            WindowsFormTextBox textBox = new WindowsFormTextBox();

            return textBox;
        }
    }
}