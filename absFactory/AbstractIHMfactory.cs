﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace absFactory
{
    public abstract class AbstractIHMfactory
    {
        public abstract AbstractButton getButton();


        public abstract AbstractTextBox getTextBox();


        public abstract AbstractCheckBox getCheckBox();

    }
}