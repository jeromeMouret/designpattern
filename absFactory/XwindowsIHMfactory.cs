﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace absFactory
{
    public class XwindowsIHMfactory : AbstractIHMfactory
    {
        public override AbstractButton getButton()
        {
            XwindowsButton button = new XwindowsButton();

            return button;
        }

        public override AbstractCheckBox getCheckBox()
        {
            XwindowsCheckBox checkButton = new XwindowsCheckBox();

            return checkButton;
        }

        public override AbstractTextBox getTextBox()
        {
            XwindowsTextBox textBox = new XwindowsTextBox();

            return textBox;
        }
    }
}